//(c) Pavlo Zvozdetskyi

// This defines the number of extruders
// :[1, 2, 3, 4, 5]
#define EXTRUDERS 1

// autoleveling option
//#define FC_USE_AUTOLEVELING
//#define FC_AUTOLEVELING_PROBE_POINTS 2   // Set the number of grid points per dimension

//#define  FC_TMC2208

/*
Presets for Flying Bear 3D printers, FC_3D_PRINTER_MODEL value

0 - OTHER, configured by this file

1 - P905 with big plate (220*280*210)

2 - P905 with small plate (220*220*210)

3 - P905H (220*220*310)

4 - P905X (242*280*350)

*/

// Chose your model
#define  FC_3D_PRINTER_MODEL 4



#if FC_3D_PRINTER_MODEL == 0

	#define MACHINE_NAME "FB P905"

	// The size of the print bed
	#define X_BED_SIZE 280
	#define Y_BED_SIZE 220
	#define Z_MAX_POS 200

#elif FC_3D_PRINTER_MODEL == 1
  #define MACHINE_NAME "FB P905"
  
  // The size of the print bed
  #define X_BED_SIZE 280
  #define Y_BED_SIZE 220
  #define Z_MAX_POS 200


#elif FC_3D_PRINTER_MODEL == 2
  #define MACHINE_NAME "FB P905"
  
  // The size of the print bed
  #define X_BED_SIZE 220
  #define Y_BED_SIZE 220
  #define Z_MAX_POS 200
#elif FC_3D_PRINTER_MODEL == 3
    #define MACHINE_NAME "FB P905H"
    
    // The size of the print bed
    #define X_BED_SIZE 220
    #define Y_BED_SIZE 220
    #define Z_MAX_POS 310
#elif FC_3D_PRINTER_MODEL == 4
    #define MACHINE_NAME "FB P905X"
    
    // The size of the print bed
    #define X_BED_SIZE 279
    #define Y_BED_SIZE 245
    #define Z_MAX_POS 350

#endif


// configuration when FC_3D_PRINTER_MODEL = 0
