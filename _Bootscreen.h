/**
 * Made with Marlin Bitmap Converter
 * http://marlinfw.org/tools/u8glib/converter.html
 *
 * This bitmap from the file 'space_vikings_title.png'
 */
#define CUSTOM_BOOTSCREEN_BMPWIDTH  128
#define CUSTOM_BOOTSCREEN_BMPHEIGHT 64
const unsigned char custom_start_bmp[] PROGMEM = {
  0x02,0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ......#..................................#......................................................................................
  0x02,0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ......#..................................#......................................................................................
  0x03,0x00,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ......##................................##......................................................................................
  0x02,0x80,0x00,0x00,0x01,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ......#.#..............................#.#......................................................................................
  0x02,0x80,0x00,0x00,0x01,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ......#.#..............................#..#.....................................................................................
  0x04,0x80,0x00,0x00,0x01,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#..#..............................#..#.....................................................................................
  0x04,0x40,0x00,0x00,0x03,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#...#............................##..#.....................................................................................
  0x04,0xC0,0x00,0x00,0x03,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#..##............................##..#.....................................................................................
  0x04,0xC0,0x00,0x00,0x03,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#..##............................##..#.....................................................................................
  0x04,0xC0,0x00,0x00,0x03,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#..##............................##..#.....................................................................................
  0x0C,0xC0,0x00,0x00,0x03,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ....##..##............................##..##....................................................................................
  0x08,0xC0,0x00,0x00,0x03,0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ....#...##............................##...#....................................................................................
  0x08,0xC0,0x00,0x00,0x01,0x10,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ....#...##.............................#...#...................#................................................................
  0x09,0x80,0x00,0x00,0x01,0x90,0x00,0x03,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ....#..##..............................##..#..................###...............................................................
  0x19,0x80,0x07,0xE0,0x01,0x88,0x00,0x1F,0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ...##..##............######............##...#..............#########............................................................
  0x11,0x80,0x1E,0x38,0x01,0xC8,0x00,0x3F,0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ...#...##..........####...###..........###..#.............##########............................................................
  0x33,0x00,0x7C,0x3E,0x00,0xCC,0x00,0x3C,0x78,0x00,0x08,0x02,0x00,0x80,0x00,0x00, // ..##..##.........#####....#####.........##..##............####...####...............#.........#.........#.......................
  0x23,0x00,0xFC,0x2F,0x00,0xC4,0x00,0x38,0x03,0xFE,0x3F,0x1F,0x07,0xC0,0x00,0x00, // ..#...##........######....#.####........##...#............###.........#########...######...#####.....#####......................
  0x61,0x01,0xE4,0x27,0x80,0x86,0x00,0x3F,0xF1,0xFF,0x7F,0xBF,0xEF,0xF8,0x00,0x00, // .##....#.......####..#....#..####.......#....##...........##########...#########.########.#########.#########...................
  0x43,0x02,0xC4,0x23,0xC0,0x42,0x00,0x1F,0xF9,0x83,0x07,0xB0,0xEC,0x78,0x00,0x00, // .#....##......#.##...#....#...####.......#....#............##########..##.....##.....####.##....###.##...####...................
  0xC2,0x05,0x84,0x20,0xA0,0x43,0x00,0x00,0xF9,0x83,0x7F,0xB0,0x0F,0xF0,0x00,0x00, // ##....#......#.##....#....#.....#.#......#....##................#####..##.....##.########.##........########....................
  0x81,0x0F,0x04,0x20,0xF0,0xC1,0x00,0x00,0x39,0x83,0xF1,0xB0,0x0F,0xC0,0x00,0x00, // #......#....####.....#....#.....####....##.....#..................###..##.....######...##.##........######......................
  0x81,0xBE,0x04,0x20,0x7D,0x81,0x00,0x1F,0xF9,0x83,0xC1,0xB0,0x2E,0x08,0x00,0x00, // #......##.#####......#....#......#####.##......#...........##########..##.....####.....##.##......#.###.....#...................
  0x81,0xFC,0x04,0x20,0x2F,0x81,0x00,0x1F,0xF9,0xFF,0xFF,0xFF,0xEF,0xF8,0x00,0x00, // #......#######.......#....#.......#.#####......#...........##########..############################.#########...................
  0x81,0x58,0x04,0x30,0x1A,0x81,0x00,0x1F,0xF1,0xFE,0x7F,0xDF,0xC7,0xF0,0x00,0x00, // #......#.#.##........#....##.......##.#.#......#...........#########...########..#########.#######...#######....................
  0xC0,0xF8,0x0C,0x30,0x1F,0x03,0x00,0x01,0x01,0x80,0x08,0x02,0x00,0x80,0x00,0x00, // ##......#####.......##....##.......#####......##...............#.......##...........#.........#.........#.......................
  0x40,0xB0,0x0C,0x10,0x0D,0x02,0x00,0x01,0x01,0x80,0x08,0x00,0x00,0x00,0x00,0x00, // .#......#.##........##.....#........##.#......#................#.......##...........#...........................................
  0x60,0xB0,0x08,0x10,0x0D,0x06,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .##.....#.##........#......#........##.#.....##................#.......#........................................................
  0x38,0xF0,0x08,0x10,0x0F,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ..###...####........#......#........####....##..................................................................................
  0x1E,0xC0,0x08,0x10,0x03,0x78,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ...####.##..........#......#..........##.####...................................................................................
  0x07,0xC0,0x08,0x10,0x03,0xE0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .....#####..........#......#..........#####.....................................................................................
  0x00,0xC0,0x08,0x10,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ........##..........#......#..........##........................................................................................
  0x00,0x40,0x08,0x10,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .........#..........#......#..........##........................................................................................
  0x00,0xC0,0x08,0x10,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ........##..........#......#..........##........................................................................................
  0x00,0xFE,0x08,0x10,0x7F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ........#######.....#......#.....#######........................................................................................
  0x00,0xDF,0xF8,0x1F,0xFB,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ........##.##########......##########.##........................................................................................
  0x00,0xB8,0x78,0x1E,0x0C,0x00,0x08,0x10,0x41,0x00,0x20,0x00,0x00,0x00,0x00,0x00, // ........#.###....####......####.....##..............#......#.....#.....#..........#.............................................
  0x00,0x80,0x0C,0x30,0x01,0x00,0x08,0x10,0xE1,0x00,0x70,0x00,0x00,0x00,0x00,0x00, // ........#...........##....##...........#............#......#....###....#.........###............................................
  0x00,0xE0,0x04,0x20,0x07,0x00,0x1E,0x3C,0xE3,0x80,0x70,0x00,0x00,0x00,0x00,0x00, // ........###..........#....#..........###...........####...####..###...###........###............................................
  0x00,0x5F,0x03,0xC0,0x7A,0x00,0x1E,0x3C,0x03,0x80,0x00,0x00,0x00,0x00,0x00,0x00, // .........#.#####......####.......####.#............####...####........###.......................................................
  0x00,0x40,0xFF,0xFF,0x02,0x00,0x1C,0x38,0x03,0x80,0x00,0x80,0x00,0x30,0x00,0x00, // .........#......################......#............###....###.........###...............#.................##....................
  0x00,0x40,0x00,0x00,0x02,0x00,0x1C,0x38,0x43,0x80,0x20,0x80,0x08,0xF0,0x40,0x00, // .........#............................#............###....###....#....###.........#.....#...........#...####.....#..............
  0x00,0x4F,0xE0,0x07,0xF2,0x00,0x1C,0x38,0xE3,0x8C,0x71,0xCE,0x1F,0x03,0xF8,0x00, // .........#..#######..........#######..#............###....###...###...###...##...###...###..###....#####......#######...........
  0x00,0x48,0x10,0x08,0x12,0x00,0x1C,0x38,0xE3,0x98,0x71,0xFE,0x3F,0x87,0xF8,0x00, // .........#..#......#........#......#..#............###....###...###...###..##....###...########...#######....########...........
  0x00,0x40,0x08,0x10,0x02,0x00,0x1E,0x38,0xE3,0xB0,0x71,0xFE,0x73,0x86,0x00,0x00, // .........#..........#......#..........#............####...###...###...###.##.....###...########..###..###....##.................
  0x00,0x60,0x04,0x20,0x06,0x00,0x1E,0x7C,0xE3,0xE0,0x71,0xC6,0x3F,0x07,0xF8,0x00, // .........##..........#....#..........##............####..#####..###...#####......###...###...##...######.....########...........
  0x00,0x28,0x04,0x20,0x14,0x00,0x1F,0xF0,0xE3,0xF0,0x71,0xC6,0x1C,0x03,0xFC,0x00, // ..........#.#........#....#........#.#.............#########....###...######.....###...###...##....###........########..........
  0x00,0x28,0x02,0x40,0x14,0x00,0x07,0xE0,0xE3,0xB8,0x71,0xC6,0x1C,0x00,0x0C,0x00, // ..........#.#.........#..#.........#.#...............######.....###...###.###....###...###...##....###..............##..........
  0x00,0x24,0x02,0x40,0x24,0x00,0x03,0xC0,0xE3,0xB8,0x71,0xC6,0x3F,0xC3,0xFC,0x00, // ..........#..#........#..#........#..#................####......###...###.###....###...###...##...########....########..........
  0x00,0x13,0x04,0x20,0xC8,0x00,0x01,0xC0,0xE3,0x9C,0x71,0xCE,0x7F,0xFB,0xF8,0x00, // ...........#..##.....#....#.....##..#..................###......###...###..###...###...###..###..############.#######...........
  0x00,0x10,0xF0,0x0F,0x08,0x00,0x01,0x80,0x41,0x0C,0x20,0x84,0x7F,0xF8,0x40,0x00, // ...........#....####........####....#..................##........#.....#....##....#.....#....#...############....#..............
  0x00,0x08,0x01,0x80,0x10,0x00,0x00,0x80,0x41,0x0E,0x20,0x80,0x1F,0xE0,0x00,0x00, // ............#..........##..........#....................#........#.....#....###...#.....#..........########.....................
  0x00,0x06,0x01,0x80,0x60,0x00,0x00,0x00,0x00,0x07,0x00,0x00,0x01,0x00,0x00,0x00, // .............##........##........##..........................................###.......................#........................
  0x00,0x01,0xC3,0xC3,0x80,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00, // ...............###....####....###.............................................##................................................
  0x00,0x00,0xA3,0xC5,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ................#.#...####...#.#................................................................................................
  0x00,0x00,0xE0,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ................###..........###................................................................................................
  0x00,0x00,0x70,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .................###........###.................................................................................................
  0x00,0x00,0x59,0x1A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // .................#.##..#...##.#.................................................................................................
  0x00,0x00,0x38,0x1C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ..................###......###..................................................................................................
  0x00,0x00,0x3E,0x7C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ..................#####..#####..................................................................................................
  0x00,0x00,0x23,0xC4,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ..................#...####...#..................................................................................................
  0x00,0x00,0x11,0x88,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ...................#...##...#...................................................................................................
  0x00,0x00,0x0C,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, // ....................##....##....................................................................................................
  0x00,0x00,0x07,0xE0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00  // .....................######.....................................................................................................
};
